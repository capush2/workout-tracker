import 'dart:developer';

import 'package:hive_flutter/hive_flutter.dart';
import 'hive_types.dart';

class BoxWrapper{
  static bool _isInitialized = false;
  static late Box _box;
  static late Activities _activities;
  static late Workouts _workouts;
  static const String _activityKey = 'key_activities';
  static const String _workoutKey = 'key_workout';
  static const String _boxName = 'mainBox';

  static initialize() async {
    _box = await Hive.openBox(_boxName);
    var activities = _box.get(_activityKey) as Activities?;
    var workouts = _box.get(_workoutKey) as Workouts?;
    if(activities == null){
      activities = Activities(names: []);
      _box.put(_activityKey, activities);
    }
    if(workouts == null){
      workouts = Workouts(workoutSets: []);
      _box.put(_workoutKey, workouts);
    }
    _activities = activities;
    _workouts = workouts;
    _isInitialized = true;
  }

  static List<String> get activities{
    throwIfNotInitialized();
    return _activities.names;
  }

  //Right now this silent fails if activityName already exists
  static void addActivity(String activityName){
    throwIfNotInitialized();
    if(!_activities.names.contains(activityName)){
      _activities.names.add(activityName);
      _activities.save();
    }
  }

  //Right now this silent fails if activityName already exists
  static void addWorkoutSet(String activityName, int repetitionCount){
    throwIfNotInitialized();
    _workouts.workoutSets.add(WorkoutSet(activityName: activityName, repetitionCount: repetitionCount, dateTime: DateTime.now()));
    _workouts.save();
    log(_workouts.toString());
  }

  static void throwIfNotInitialized(){
    if(!_isInitialized){
      throw Exception("Hive box was not initialized through method BoxWrapper.initialize().");
    }
  }

}