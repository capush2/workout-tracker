import 'package:flutter/material.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:workout/hive_types.dart';

import 'box_wrapper.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(ActivitiesAdapter());
  Hive.registerAdapter(WorkoutsAdapter());
  Hive.registerAdapter(WorkoutSetAdapter());
  await BoxWrapper.initialize();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.teal),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Workout tracker'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _currentActivity = "";
  TextEditingController newActivityController = TextEditingController();
  TextEditingController newWorkoutController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 30),
              child: TextField(
                decoration: const InputDecoration(
                  labelText: 'New activity type',
                ),
                keyboardType: TextInputType.text,
                controller: newActivityController,
                onSubmitted: (String activityName) => {
                  setState(() {
                    BoxWrapper.addActivity(activityName);
                    newActivityController.clear();
                  })
                },
              ),
            ),
            const SizedBox(height: 100),
            Center(
              child: DropdownMenu(
                width: 200,
                dropdownMenuEntries: BoxWrapper.activities
                    .map((value) => DropdownMenuEntry(value: value, label: value))
                    .toList(),
                initialSelection: _currentActivity,
                onSelected: (String? s) => {_currentActivity = s!},
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 30),
              child: TextField(
                decoration: const InputDecoration(
                  labelText: 'Submit repetitions',
                ),
                keyboardType: TextInputType.number,
                controller: newWorkoutController,
                onSubmitted: (String repetitionCount) => {
                  setState(() {
                    BoxWrapper.addWorkoutSet(
                        _currentActivity, int.parse(repetitionCount));
                    newWorkoutController.clear();
                  })
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
