import 'package:hive/hive.dart';
import 'package:intl/intl.dart';

part 'hive_types.g.dart';

@HiveType(typeId: 1)
class Activities extends HiveObject{
  Activities({required this.names});

  @HiveField(0)
  List<String> names;
}

@HiveType(typeId: 2)
class Workouts extends HiveObject{
  Workouts({required this.workoutSets});

  @HiveField(0)
  List<WorkoutSet> workoutSets;

  @override
  String toString() {
    String s = '\n';
    Set<String> uniqueActivities = {...workoutSets.map((e) => e.activityName)};
    for(String activity in uniqueActivities){
      s += '\n'
          '_________________________________________________\n'
          '$activity\n\n';
      for(WorkoutSet set in workoutSets.where((e) => e.activityName == activity)){
        s += '$set\n';
      }
    }
    return s;
  }
}

@HiveType(typeId: 3)
class WorkoutSet extends HiveObject{
  WorkoutSet({required this.activityName, required this.repetitionCount, required this.dateTime});

  @HiveField(0)
  String activityName;

  @HiveField(1)
  int repetitionCount;

  @HiveField(2)
  DateTime dateTime;

  @override
  String toString() {
    return '${DateFormat('yyyy-MM-dd hh:mm').format(dateTime)} : $repetitionCount';
  }
}