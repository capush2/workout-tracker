// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hive_types.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ActivitiesAdapter extends TypeAdapter<Activities> {
  @override
  final int typeId = 1;

  @override
  Activities read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Activities(
      names: (fields[0] as List).cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, Activities obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.names);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ActivitiesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class WorkoutsAdapter extends TypeAdapter<Workouts> {
  @override
  final int typeId = 2;

  @override
  Workouts read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Workouts(
      workoutSets: (fields[0] as List).cast<WorkoutSet>(),
    );
  }

  @override
  void write(BinaryWriter writer, Workouts obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.workoutSets);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WorkoutsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class WorkoutSetAdapter extends TypeAdapter<WorkoutSet> {
  @override
  final int typeId = 3;

  @override
  WorkoutSet read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return WorkoutSet(
      activityName: fields[0] as String,
      repetitionCount: fields[1] as int,
      dateTime: fields[2] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, WorkoutSet obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.activityName)
      ..writeByte(1)
      ..write(obj.repetitionCount)
      ..writeByte(2)
      ..write(obj.dateTime);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WorkoutSetAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
